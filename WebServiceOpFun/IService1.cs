﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WebServiceOpFun
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IService1" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IService1
    {

        [OperationContract]
        string suma(int v1, int v2);
        [OperationContract]
        string resta(int v1, int v2);
        [OperationContract]
        string multiplicacion(int v1, int v2);
        [OperationContract]
        string division(int v1, int v2);
    }
}